<?php

namespace src;

class Constants 
{
    public static $txtDirectory = 'txt';
    public static $outputDirectory = 'results';
    public static $tmpDirectory = 'tmp';
    public static $cvDirectory = 'cv';

    public static $pdfFont = 'Arial';

    public static $all = 'all';
    public static $file = 'file';

    public static $pdfExtension = '.pdf';
    public static $FPDFDefaultOutput = 'F';
}
