<?php

require ('./vendor/autoload.php');
require ('./src/AbstractScanner.php');
require ('./src/TxtScanner.php');
require ('./src/Constants.php');
require ('./src/PdfCreator.php');
require ('./src/TxtToPdf.php');
require ('./src/Merger.php');
require ('./src/SimpleScanner.php');

use \src\SimpleScanner;
use \src\TxtScanner;
use \src\Constants;
use \src\PdfCreator;
use \src\TxtToPdf;
use \src\Merger;

new Merger();
