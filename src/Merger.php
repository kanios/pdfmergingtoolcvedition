<?php

namespace src;

class Merger
{
    public function __construct()
    {
        $this->prepareFilesToMerge();
        $this->goGoPowerRangers();
    }

    private function prepareFilesToMerge() { new TxtToPdf(); }

    private function goGoPowerRangers()
    {
        $cvDir = new SimpleScanner(Constants::$cvDirectory);
        $tmpDir = new SimpleScanner(Constants::$tmpDirectory);

        foreach ($cvDir->getFiles() as $cv)
        {
            foreach ($tmpDir->getFiles() as $tmp)
            {
                $newFileName = substr($cv, 0, -4) . "_$tmp";
                $this->concatPDFs($newFileName, $cv, $tmp);
            }
        }

        $this->clearTmp();
    }

    private function concatPDFs($name, $cv, $tmp)
    {
        $pdf = new \PDFMerger();
        $pdf->addPDF(Constants::$cvDirectory . '/' . $cv, Constants::$all);
        $pdf->addPDF(Constants::$tmpDirectory . '/' . $tmp, Constants::$all);
        $pdf->merge(Constants::$file, Constants::$outputDirectory . '/' . $name);
    }

    private function clearTmp()
    {
        $files = glob('tmp/*');

        foreach ($files as $file)
            if (is_file($file)) unlink($file);
    }
}
