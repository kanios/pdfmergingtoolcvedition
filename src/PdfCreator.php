<?php

namespace src;

class PdfCreator extends \FPDF
{
    public function create($filename, $txt)
    {
        $filename .= Constants::$pdfExtension;

        $this->AddPage();
        $this->SetFont(Constants::$pdfFont, '', 12);
        $this->Cell(40, 80, $txt);   
        $this->Output('F', $filename);

        $tmp = Constants::$tmpDirectory;
        rename("./$filename", "./$tmp/$filename");
    }
}
