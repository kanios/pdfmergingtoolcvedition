README:

1. Do folderu txt/ wrzucasz kolejne pliki tekstowe z odpowiednimi klauzulami nazywając je nazwami firm, do których chcesz złożyć CV

2. Do folderu cv/ wrzucasz swoje przygotowane wcześniej CV (możesz kilka)

3. Do konsoli wpisujesz php run.php

4. Program wygeneruje gotowe CV z klauzulami do folderu results/ o nazwach {NAZWA_CV}_{NAZWA_FIRMY}.pdf
