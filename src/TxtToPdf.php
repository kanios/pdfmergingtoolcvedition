<?php

namespace src;

class TxtToPdf
{
    public function __construct()
    {
        $scan = new TxtScanner();
        $body = $scan->getData();

        foreach ($body as $filename => $data)
        {
            $pdf = new PdfCreator();
            $pdf->create($filename, $data);
            unset($pdf);
        }
    }
}
