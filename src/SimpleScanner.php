<?php

namespace src;

class SimpleScanner extends Scanner
{
    public function __construct($dirName)
    {
        $this->pathToDir = $dirName;
        $this->files = $this->scanDirectory();
    }

    public function getFiles() { return $this->files; }
}
