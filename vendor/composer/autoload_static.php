<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit1ce8df286be8b7f14c83929ae592a9b1
{
    public static $prefixesPsr0 = array (
        'P' => 
        array (
            'PDFMerger' => 
            array (
                0 => __DIR__ . '/..' . '/rguedes/pdfmerger/Classes',
            ),
        ),
    );

    public static $classMap = array (
        'FPDF' => __DIR__ . '/..' . '/setasign/fpdf/fpdf.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixesPsr0 = ComposerStaticInit1ce8df286be8b7f14c83929ae592a9b1::$prefixesPsr0;
            $loader->classMap = ComposerStaticInit1ce8df286be8b7f14c83929ae592a9b1::$classMap;

        }, null, ClassLoader::class);
    }
}
