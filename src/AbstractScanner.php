<?php

namespace src;

abstract class Scanner
{
    protected $data = array();
    protected $files = array();
    protected $pathToDir;

    public function getData() { return $this->data; }

    protected function scanDirectory()
    {
        return array_filter(scandir($this->pathToDir), function($item) {
            return !in_array($item[0], array('.', '..'));
        });
    }
}
