<?php

namespace src;

class TxtScanner extends Scanner
{
    public function __construct()
    {
        $this->pathToDir = Constants::$txtDirectory;
        $this->files = $this->scanDirectory();
        $this->data = $this->getTxtDataFromFiles();
    }

    private function getTxtDataFromFiles()
    {
        $agreementsArray = array();

        foreach ($this->files as $key => $file)
        {
            $body = file_get_contents($this->pathToDir . '/' . $file);
            if (!empty($body)) $agreementsArray[substr($file, 0, -4)] = $body;
        }

        return $agreementsArray;
    }
}
